# QT-ROBOCUP-TEMPLATE
Base de développement pour la solution Qt de pilotage du robot.                    

![alt text](img/screenshot.png "Qt App screenshot")

Cette solution est constituée de 2 applications logicielles :

1. une application _Qt Widgets_ `QRobotGUI` qui :

- constitue l'interface homme-machine du robot

- s'exécute sur un PC Linux

- représente un client réseau TCP vers la 2ème application 

2. une application _Qt console_ `QRobotController` qui :

- s'exécute sur la Raspberry Pi présente sur le chassis du robot 

- reçoit depuis la 1ère application les ordres de pilotage du robot via une connexion réseau TCP

- relaye ces ordres par liaison série vers la carte Arduino présente également sur le chassis du robot afin qu'elle génère les signaux nécessaires pour piloter les différents moteurs

> **_NOTE:_**  `QRobotGUI` présente 2 limitations.
Elle doit être exécutée sur _Linux_ de façon à pouvoir prendre en charge le Gamepad via la librairie _QGamepad_ (paquets `libQt5Gamepad5` et `libqt5-qtgamepad-devel` à installer au préalable sur _OpenSUSE_).
D'autre part, elle doit être exécutée sur un _Linux_ natif et non sous forme de machine virtuelle si on souhaite piloter le robot par le clavier au lieu du Gamepad (pour cause de soucis au niveau de l'auto-repeat des touches -> voir commentaire dans `InputDevice.h`)

## Protocoles

2 protocoles ont été élaborés pour la communication TCP :

- Le "protocole TCP" entre `QRobotGUI` et le `QRobotController`
- le "procotocle série" entre `QRobotController` et la carte Arduino d'autre part.

### Protocole TCP

* Pilotage chassis :

| **début de trame** | **commande** | **sous-commande** | **checksum** | **fin de trame** | **action** |
|:---:|:---:|:---:|:---:|:---:|:---:|
| 0x02 (\<_STX_>) | 0x52 ('R') | 0x46 ('F') | 0x68 | 0x03 (\<_ETX_>) | Avancer |
| 0x02 (\<_STX_>) | 0x52 ('R') | 0x42 ('B') | 0x6C | 0x03 (\<_ETX_>) | Reculer |
| 0x02 (\<_STX_>) | 0x52 ('R') | 0x4C ('L') | 0x62 | 0x03 (\<_ETX_>) | Tourner à gauche |
| 0x02 (\<_STX_>) | 0x52 ('R') | 0x52 ('R') | 0x5C | 0x03 (\<_ETX_>) | Tourner à droite |
| 0x02 (\<_STX_>) | 0x52 ('R') | 0x53 ('S') | 0x5B | 0x03 (\<_ETX_>) | Stopper |


* Pilotage Pince

| **début de trame** | **commande** | **sous-commande** | **checksum** | **fin de trame** | **action** |
|:---:|:---:|:---:|:---:|:---:|:---:|
| 0x02 (\<_STX_>) | 0x43 ('C') | 0x4F ('O')) | 0x6E | 0x03 (\<_ETX_>) | Ouvrir |
| 0x02 (\<_STX_>) | 0x43 ('C') | 0x43 ('C') | 0x7A | 0x03 (\<_ETX_>) | Fermer |
| 0x02 (\<_STX_>) | 0x43 ('C') | 0x53 ('S') | 0x6A | 0x03 (\<_ETX_>) | Stopper |


Chaque trame valide est acquittée par le serveur avec \<ACK> (0x06). Dans le cas d'une trame non valide (erreur checksum, commande ou sous-commande inconnue) rien n'est renvoyé.

### Protocole série

* Pilotage chassis :

**commande** | **action** |
|:---:|:---:|
| 0x41 ('A') | Avancer |
| 0x52 ('R') | Reculer |
| 0x4C ('L') | Tourner à gauche |
| 0x52 ('R') | Tourner à droite |
| 0x53 ('S') | Stopper |

* Pilotage Pince

**commande** | **action** |
|:---:|:---:|
| 0x4F ('O')) | Ouvrir |
| 0x43 ('C') | Fermer |
| 0x48 ('H') | Stopper |

Aucun accusé de réception n'est attendu.
