#include <QMessageBox>

#include "maingui.h"
#include "ui_maingui.h"

MainGUI::MainGUI(QWidget *parent)
    : QDialog(parent)
    , ui(new Ui::MainGUI)
{
    ui->setupUi(this);

    _input = new InputDevice(this);

    connect(_input, &InputDevice::goForward, this, &MainGUI::onRobotGoForward);
    connect(_input, &InputDevice::goBackward, this, &MainGUI::onRobotGoBackward);
    connect(_input, &InputDevice::turnLeft, this, &MainGUI::onRobotTurnLeft);
    connect(_input, &InputDevice::turnRight, this, &MainGUI::onRobotTurnRight);
    connect(_input, &InputDevice::stopRobot, this, &MainGUI::onRobotStop);

    connect(_input, &InputDevice::closeClamp, this, &MainGUI::onClampClose);
    connect(_input, &InputDevice::openClamp, this, &MainGUI::onClampOpen);
    connect(_input, &InputDevice::stopClamp, this, &MainGUI::onClampStop);

    _input->setFocus();
}

MainGUI::~MainGUI()
{
    delete ui;
}

void MainGUI::onRobotGoForward()
{
    ui->lblRobotPlayPause->setStyleSheet(QString::fromUtf8("border-image: url(:/img/play.svg);"));
    ui->lblRobotForward->setStyleSheet(QString::fromUtf8("border-image: url(:/img/up.svg);background-color: rgb(0, 170, 0);"));

    _client->go(TcpRobotControllerClient::direction_t::FORWARD);

}

void MainGUI::onRobotGoBackward()
{
    ui->lblRobotPlayPause->setStyleSheet(QString::fromUtf8("border-image: url(:/img/play.svg);"));
    ui->lblRobotBackward->setStyleSheet(QString::fromUtf8("border-image: url(:/img/down.svg);background-color: rgb(0, 170, 0);"));

    _client->go(TcpRobotControllerClient::direction_t::BACKWARD);
}

void MainGUI::onRobotTurnLeft()
{
    ui->lblRobotPlayPause->setStyleSheet(QString::fromUtf8("border-image: url(:/img/play.svg);"));
    ui->lblRobotTurnLeft->setStyleSheet(QString::fromUtf8("border-image: url(:/img/left.svg);background-color: rgb(0, 170, 0);"));

    _client->go(TcpRobotControllerClient::direction_t::LEFT);
}

void MainGUI::onRobotTurnRight()
{
    ui->lblRobotPlayPause->setStyleSheet(QString::fromUtf8("border-image: url(:/img/play.svg);"));
    ui->lblRobotTurnRight->setStyleSheet(QString::fromUtf8("border-image: url(:/img/right.svg);background-color: rgb(0, 170, 0);"));

    _client->go(TcpRobotControllerClient::direction_t::RIGHT);
}

void MainGUI::onRobotStop()
{
    ui->lblRobotPlayPause->setStyleSheet(QString::fromUtf8("border-image: url(:/img/pause.svg);"));
    ui->lblRobotForward->setStyleSheet(QString::fromUtf8("border-image: url(:/img/up.svg);background-color: none;"));
    ui->lblRobotBackward->setStyleSheet(QString::fromUtf8("border-image: url(:/img/down.svg);background-color: none;"));
    ui->lblRobotTurnLeft->setStyleSheet(QString::fromUtf8("border-image: url(:/img/left.svg);background-color: none;"));
    ui->lblRobotTurnRight->setStyleSheet(QString::fromUtf8("border-image: url(:/img/right.svg);background-color: none;"));

    _client->go(TcpRobotControllerClient::direction_t::STOP);
}

void MainGUI::onClampOpen()
{
    ui->lblClampPlayPause->setStyleSheet(QString::fromUtf8("border-image: url(:/img/play.svg);"));
    ui->lblClampOpen->setStyleSheet(QString::fromUtf8("border-image: url(:/img/clamp-open.svg);background-color: rgb(0, 170, 0);"));

    _client->driveClamp(TcpRobotControllerClient::action_t::OPEN);
}

void MainGUI::onClampClose()
{
    ui->lblClampPlayPause->setStyleSheet(QString::fromUtf8("border-image: url(:/img/play.svg);"));
    ui->lblClampClose->setStyleSheet(QString::fromUtf8("border-image: url(:/img/clamp-close.svg);background-color: rgb(0, 170, 0);"));

    _client->driveClamp(TcpRobotControllerClient::action_t::CLOSE);
}

void MainGUI::onClampStop()
{
    ui->lblClampPlayPause->setStyleSheet(QString::fromUtf8("border-image: url(:/img/pause.svg);"));
    ui->lblClampClose->setStyleSheet(QString::fromUtf8("border-image: url(:/img/clamp-close.svg);background-color: none;"));
    ui->lblClampOpen->setStyleSheet(QString::fromUtf8("border-image: url(:/img/clamp-open.svg);background-color: none;"));

    _client->driveClamp(TcpRobotControllerClient::action_t::STOP);
}


void MainGUI::on_btnConnect_clicked()
{
    QHostAddress host;
    bool okIp;

    if(ui->btnConnect->isChecked()) { /* Connecter */
        okIp = host.setAddress(ui->ledtServerIP->text());

        if(okIp) {
            _client = new TcpRobotControllerClient(host);
            connect(_client, &TcpRobotControllerClient::connected
                    , this, &MainGUI::onTcpClientConnected
                    );
            connect(_client, &TcpRobotControllerClient::ackReceived
                    , this, &MainGUI::onTcpClientAckReceived
                    );
            connect(_client, &TcpRobotControllerClient::errorOccured
                    , this, &MainGUI::onTcpClientErrorOccured
                    );
            ui->ledtServerIP->setEnabled(false);
        } else {
            QMessageBox::warning(this, "Erreur", "Adresse IP non valide");
        }
    } else { /* Déconnecter */
//        if(_input != nullptr) {
//            delete _input;
//            _input = nullptr;
//        }
        if(_client != nullptr) {
            delete _client;
            _client = nullptr;
        }
        // MàJ IHM
        ui->btnConnect->setText("Connecter");
        ui->ledtServerIP->setEnabled(true);
        ui->frmPS3->setEnabled(false);
    }
}

void MainGUI::onTcpClientConnected()
{
    // MàJ IHM
    ui->btnConnect->setText("Déconnecter");

    ui->ledtServerIP->setEnabled(false);

    ui->frmPS3->setEnabled(true);


}

void MainGUI::onTcpClientAckReceived()
{
    qDebug() << "Message acquitté par le serveur";
}

void MainGUI::onTcpClientErrorOccured(QString err)
{
    QMessageBox::warning(this, "Avertissement", err);
    setEnabled(true);

//    if(_input != nullptr) {
//        delete _input;
//        _input = nullptr;
//    }
    ui->frmPS3->setEnabled(false);

    ui->lblRobotPlayPause->setStyleSheet(QString::fromUtf8("border-image: url(:/img/pause.svg);"));
    ui->lblRobotForward->setStyleSheet(QString::fromUtf8("border-image: url(:/img/up.svg);background-color: none;"));
    ui->lblRobotBackward->setStyleSheet(QString::fromUtf8("border-image: url(:/img/down.svg);background-color: none;"));
    ui->lblRobotTurnLeft->setStyleSheet(QString::fromUtf8("border-image: url(:/img/left.svg);background-color: none;"));
    ui->lblRobotTurnRight->setStyleSheet(QString::fromUtf8("border-image: url(:/img/right.svg);background-color: none;"));

    ui->lblClampPlayPause->setStyleSheet(QString::fromUtf8("border-image: url(:/img/pause.svg);"));
    ui->lblClampClose->setStyleSheet(QString::fromUtf8("border-image: url(:/img/clamp-close.svg);background-color: none;"));
    ui->lblClampOpen->setStyleSheet(QString::fromUtf8("border-image: url(:/img/clamp-open.svg);background-color: none;"));

    ui->btnConnect->setChecked(false);
    on_btnConnect_clicked();
}

