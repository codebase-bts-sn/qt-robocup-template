/**
 * Classe de gestion du périphérique de contrôle du robot et de la pince (Gamepad ou Clavier)
 *
 * !! ATTENTION !! La prise en charge du clavier présente un problème lorsque l'application
 * est exécutée dans une machine virtuelle Linux dans VMWare : le traitement de l'auto-repeat
 * ne fonctionne pas. Ainsi, lorsqu'on reste appuyé sur une des touches actives, des évènements
 * keyReleaseEvent suivis d'évènements keyPressEvent sont délivrés en permanence et ne sont pas reconnus
 * comme des évènements répétitif par la fonction Qt isAutoRepeat().
 */
#ifndef INPUTDEVICE_H
#define INPUTDEVICE_H

#include <QWidget>
#include <QKeyEvent>
#include <QGamepad>

class InputDevice : public QWidget
{
    Q_OBJECT
public:
    explicit InputDevice(QWidget *parent = nullptr);

signals:
    void goForward();
    void goBackward();
    void turnLeft();
    void turnRight();
    void stopRobot();
    void openClamp();
    void closeClamp();
    void stopClamp();

protected:
    void keyPressEvent(QKeyEvent * evt) override;
    void keyReleaseEvent(QKeyEvent * evt) override;

private:
  QGamepad * _gamepad;

};

#endif // INPUTDEVICE_H
