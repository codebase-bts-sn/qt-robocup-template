#ifndef MAINGUI_H
#define MAINGUI_H

#include <QDialog>
#include <inputdevice.h>
#include <tcprobotcontrollerclient.h>

QT_BEGIN_NAMESPACE
namespace Ui { class MainGUI; }
QT_END_NAMESPACE

class MainGUI : public QDialog
{
    Q_OBJECT

public:
    MainGUI(QWidget *parent = nullptr);
    ~MainGUI();

private:
    Ui::MainGUI *ui;
    InputDevice * _input = nullptr;
    TcpRobotControllerClient * _client = nullptr;

private slots:
    // Slots gamepad/clavier
    void onRobotGoForward();
    void onRobotGoBackward();
    void onRobotTurnLeft();
    void onRobotTurnRight();
    void onRobotStop();
    void onClampOpen();
    void onClampClose();
    void onClampStop();

    // Slots IHM
    void on_btnConnect_clicked();

    // Slots client TCP
    void onTcpClientConnected();
    void onTcpClientAckReceived();
    void onTcpClientErrorOccured(QString err);
};
#endif // MAINGUI_H
