#include <QMessageBox>
#include <QDebug>


#include "inputdevice.h"

InputDevice::InputDevice(QWidget *parent) : QWidget(parent)
{
    auto gamepads = QGamepadManager::instance()->connectedGamepads();

    // Accepter le focus par touche TAB ou Clic souris de façon à pouvoir traiter les touches clavier
    setFocusPolicy(Qt::StrongFocus);

    if (gamepads.isEmpty()) {
        qDebug() << "Pas de joystick trouvé";
        QMessageBox::warning(this, "Avertissement", "Pas de joystick trouvé !\n"
            "=> Utilisation du clavier uniquement\n"
            "* Robot :\n"
            "Z:Avancer / S:Reculer / Q:Gauche / D:Droit\n"
            "* Pince :\n"
            "A:Fermer / E:Ouvrir"
            );
        _gamepad = nullptr;
    } else {
        _gamepad = new QGamepad(*gamepads.begin(), this);
        connect(_gamepad, &QGamepad::axisLeftXChanged, this, [=](double value){
            qDebug() << "Left X" << value;
            if(value == -1) {
                emit turnLeft();
            } else if(value == +1) {
                emit turnRight();
            } else {
                emit stopRobot();
            }
        });
        connect(_gamepad, &QGamepad::axisLeftYChanged, this, [=](double value){
            qDebug() << "Left Y" << value;
            if(value == -1) {
                emit goForward();
            } else if(value == +1) {
                emit goBackward();
            } else {
                emit stopRobot();
            }
        });
        connect(_gamepad, &QGamepad::buttonBChanged, this, [=](bool pressed){
            qDebug() << "Button B" << pressed;
            if(pressed) {
                emit openClamp();
            } else {
                emit stopClamp();
            }
        });
        connect(_gamepad, &QGamepad::buttonXChanged, this, [=](bool pressed){
            qDebug() << "Button X" << pressed;
            if(pressed) {
                emit closeClamp();
            } else {
                emit stopClamp();
            }
        });
    }
}

void InputDevice::keyPressEvent(QKeyEvent *evt)
{
    static int count;
    bool grabKbd = false;

    if( ! evt->isAutoRepeat() ) {
        qDebug() << "real keypress (" << count++ << ") => processing...";
        switch( evt->key() ) {
        case Qt::Key_Up :
        case Qt::Key_Z :
            //onRobotGoForward();
            emit goForward();
            grabKbd = true;
            break;
        case Qt::Key_Down :
        case Qt::Key_S :
            //onRobotGoBackward();
            emit goBackward();
            grabKbd = true;
            break;
        case Qt::Key_Left :
        case Qt::Key_Q :
            //onRobotTurnLeft();
            emit turnLeft();
            grabKbd = true;
            break;
        case Qt::Key_Right :
        case Qt::Key_D :
            //onRobotTurnRight();
            emit turnRight();
            grabKbd = true;
            break;
        case Qt::Key_A :
            //onClampClose();
            emit closeClamp();
            grabKbd = true;
            break;
        case Qt::Key_E :
            //onClampOpen();
            emit openClamp();
            grabKbd = true;
            break;
        default :
            QWidget::keyPressEvent(evt);
            break;
        }
        // SI touche de commande robot/pince enfoncée ALORS
        if(grabKbd) {
            qDebug() << "touche enfoncée";
            // Interdire aux autres widgets d'intercepter les évènements clavier
            grabKeyboard();
        }
    } else {
        //qDebug() << "auto-repeat keypress => ignoring...";
        //QWidget::keyPressEvent(evt);
        evt->ignore();
    }
}


void InputDevice::keyReleaseEvent(QKeyEvent *evt)
{
    bool releaseKbd = false;

    static int count = 0;
    if( ! evt->isAutoRepeat() ) {
        qDebug() << "real keyrelease (" << count++ << ") => processing...";
        switch( evt->key() ) {
        case Qt::Key_Up :
        case Qt::Key_Z :
        case Qt::Key_Down :
        case Qt::Key_S :
        case Qt::Key_Left :
        case Qt::Key_Q :
        case Qt::Key_Right :
        case Qt::Key_D :
            //onRobotStop();
            emit stopRobot();
            releaseKbd = true;
            break;
        case Qt::Key_A :
        case Qt::Key_E :
            //onClampStop();
            emit stopClamp();
            releaseKbd = true;
            break;
        default :
            QWidget::keyPressEvent(evt);
            break;
        }
        // SI touche de commande robot/pince relâchée ALORS
        if(releaseKbd) {
            qDebug() << "touche relachée";
            // Rendre de nouveau accès au évènements clavier aux autres widgets
            releaseKeyboard();
        }
    } else {
        //qDebug() << "auto-repeat keyrelease => ignoring...";
        //QWidget::keyPressEvent(evt);
        evt->ignore();
    }
}

