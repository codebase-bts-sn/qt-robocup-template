#include <QtGlobal>
#include "tcprobotcontrollerclient.h"

TcpRobotControllerClient::TcpRobotControllerClient(QHostAddress host, QObject *parent)
    : QObject(parent), _hostIp(host)
{
    _tmr.setSingleShot(true);

     connect(&_tcpSocket, &QTcpSocket::connected, this, &TcpRobotControllerClient::onTcpConnected);
     connect(&_tcpSocket, &QTcpSocket::readyRead, this, &TcpRobotControllerClient::onTcpReadyRead);
 #if QT_VERSION >= QT_VERSION_CHECK(5,15,0)
     connect(&_tcpSocket, &QTcpSocket::errorOccured, this, &TcpRobotControllerClient::onTcpErrorOccured);
 #else
     connect(&_tcpSocket, QOverload<QAbstractSocket::SocketError>::of(&QAbstractSocket::error),
         [=](){
             emit errorOccured(_tcpSocket.errorString());
     });
 #endif
     connect(&_tmr, &QTimer::timeout, this, &TcpRobotControllerClient::onTmrTimeOut);

    // Se connecter au serveur
     _tcpSocket.connectToHost(_hostIp, _port);
}

TcpRobotControllerClient::~TcpRobotControllerClient()
{
    _tcpSocket.disconnectFromHost();
}

void TcpRobotControllerClient::go(TcpRobotControllerClient::direction_t dir)
{
    char direction = static_cast<char>(dir);

    _bufOut.clear();

    // Préparer le message à envoyer (<STX>R<dir><CRC><ETX> avec <CRC> = 'R' ^ ['F' | 'B' | 'L' | 'R']) :
    // * début de trame
    _bufOut.append(QByteArray("\x02R"));

    // * direction
    _bufOut.append(direction);

    // * checksum (complément à 2 de la somme des caractères de la commande)
    _bufOut.append( -('R' + direction));

    // * fin de trame
    _bufOut.append(0x03);

    // Envoi de la trame sur le serveur
    send();
}

void TcpRobotControllerClient::stop()
{
    go(direction_t::STOP);
}

void TcpRobotControllerClient::driveClamp(TcpRobotControllerClient::action_t act)
{
    char action = static_cast<char>(act);

    _bufOut.clear();

    // Préparer le message à envoyer (<STX>C<action><CRC><ETX> avec <CRC> = 'C' ^ ['O' | 'C' | 'F']) :
    // * début de trame
    _bufOut.append(QByteArray("\x02""C")); /* Attention : "\x02C" est interprété comme "\x2C"
                                             et non comme 0x02 suivi de 'C' car 'C' est un
                                            symbole hexadécimal valide */

    // * direction
    _bufOut.append(action);

    // * checksum (complément à 2 de la somme des caractères de la commande)
    _bufOut.append( -('C' + action));

    // * fin de trame
    _bufOut.append(0x03);

    // Envoi de la trame sur le serveur
    send();
}

void TcpRobotControllerClient::send()
{
    // Effacer buffer de réception
    _bufIn.clear();

    // Envoyer le message contenu dans le buffer
    _tcpSocket.write(_bufOut);

    _tmr.start(_TCP_TIMEOUT);
}


void TcpRobotControllerClient::onTcpConnected()
{
    qDebug() << "Connecté au serveur";
    emit connected();
}

void TcpRobotControllerClient::onTcpReadyRead()
{
    _bufIn.append(_tcpSocket.readAll());

    if(_bufIn.contains(QByteArray("\x06"))) {
        _bufIn.clear();
        _tmr.stop();
        emit ackReceived();
    }
}

#if QT_VERSION >= QT_VERSION_CHECK(5,15,0)
void TcpRobotControllerClient::onTcpErrorOccured()
{
    emit errorOccured(_tcpSocket.errorString());
}
#endif

void TcpRobotControllerClient::onTmrTimeOut()
{
    _tmr.stop();
    emit errorOccured("Délai d'attente réponse dépassé");
}

