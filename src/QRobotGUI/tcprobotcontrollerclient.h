#ifndef TCPROBOTCONTROLLERCLIENT_H
#define TCPROBOTCONTROLLERCLIENT_H

#include <QObject>
#include <QTcpSocket>
#include <QHostAddress>
#include <QDataStream>
#include <QTimer>

class TcpRobotControllerClient : public QObject
{
    Q_OBJECT
public:
    TcpRobotControllerClient(QHostAddress host, QObject *parent=nullptr);
    ~TcpRobotControllerClient();
    enum class direction_t {FORWARD='F', BACKWARD='B', LEFT='L', RIGHT='R', STOP='S'} ;
    void go(direction_t dir);
    void stop();
    enum class action_t {OPEN='O', CLOSE='C', STOP='S'} ;
    void driveClamp(action_t act);

signals:
    void connected();
    void ackReceived();
    void errorOccured(QString err);

private slots:
    void onTcpConnected();
    void onTcpReadyRead();
#if QT_VERSION >= QT_VERSION_CHECK(5,15,0)
      void onTcpErrorOccured();
#endif
    void onTmrTimeOut();

private:
    QTcpSocket _tcpSocket;
    const QHostAddress _hostIp;
    const int _port = 1234;
    void handleError(QAbstractSocket::SocketError);
    QDataStream _in;
    QByteArray _bufOut;
    QByteArray _bufIn;
    QTimer _tmr;
    const int _TCP_TIMEOUT = 5000; //ms
    void send();
};

#endif // TCPROBOTCONTROLLERCLIENT_H
