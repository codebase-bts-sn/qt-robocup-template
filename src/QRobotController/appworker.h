#ifndef APPWORKER_H
#define APPWORKER_H

#include <QObject>
#include <QTcpServer>
#include <QTcpSocket>

#include <serialport.h>

class AppWorker : public QObject
{
    Q_OBJECT
public:
    explicit AppWorker(QObject *parent = nullptr);

private :
    SerialPort * _ser;
    QTcpServer _monServeurTCP;
    QTcpSocket * _client = nullptr;

public slots:
    void demarrer();
    void quitter();

private slots:
    void onReceptionTrame(QString trame);
    void onNewConnection();
    void onReadyRead();
    void onSocketStateChanged(QAbstractSocket::SocketState socketState);
signals:
    void termine();
};

#endif // APPWORKER_H
