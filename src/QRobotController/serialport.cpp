#include <QDebug>

#include "serialport.h"

SerialPort::SerialPort(QString serialPortName, QObject *parent) : QObject(parent)
{
    // Remanie le nom du port si port série virtuel (-> Com0Com)
    if(serialPortName.contains("CNC")) {
        serialPortName.insert(0,"\\\\.\\");
    }

    // Crée un objet associé au port série dont le nom
    // a été fourni lors de l'appel au constructeur
    _serial = new QSerialPort(serialPortName);

    // Configure le port série
    _serial->setBaudRate(QSerialPort::Baud9600);
    _serial->setDataBits(QSerialPort::Data8);
    _serial->setStopBits(QSerialPort::OneStop);
    _serial->setFlowControl(QSerialPort::NoFlowControl);

    // Met en place la fonction responsable d'intercepter
    // les caractères reçus
    connect(_serial, &QSerialPort::readyRead, this, &SerialPort::onReadyRead);

    // Initialise le buffer de réception
    _bufReception.clear();

    // Ouvre le port série en entrée et en sortie
    _serial->open(QIODevice::ReadWrite);

}

void SerialPort::send(QString data)
{
    _serial->write(data.toUtf8().append('\r'));
}

void SerialPort::onReadyRead()
{
    QByteArray response;

    // Lit les caractères reçus et les ajoute au buffer
    // de réception
    _bufReception.append(_serial->readAll());

    // SI buffer contient un '\r' (retour chariot) ALORS
    if( _bufReception.contains('\r') ) {
        // Déterminer position de '\r'
        int idxEndFrame = _bufReception.indexOf('\r');
        // Extraire la trame délimitée par '\r' du buffer de réception
        response = _bufReception.left(idxEndFrame);
        // Supprimer la trame extraite du buffer de réception
        _bufReception.remove(0, idxEndFrame+1);
        // Publier la trame reçue
        emit receptionTrame(QString::fromLatin1(response));
    }
}
