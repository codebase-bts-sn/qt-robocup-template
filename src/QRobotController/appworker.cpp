#include <iostream>

#include <QCoreApplication>
#include <QSerialPortInfo>
#include <QRegExp>
#include <QDebug>

#include "appworker.h"

using namespace std;

AppWorker::AppWorker(QObject *parent) : QObject(parent)
{
}

void AppWorker::demarrer()
{
    // Initialise port série
    _ser = new SerialPort("/tmp/ttySocat0");

    QObject::connect(_ser, &SerialPort::receptionTrame, this, &AppWorker::onReceptionTrame);

    // Initialise serveur
    qDebug() << "Serveur en écoute";
    _monServeurTCP.listen(QHostAddress::Any, 1234);
    QObject::connect(&_monServeurTCP, &QTcpServer::newConnection, this, &AppWorker::onNewConnection);
}

void AppWorker::quitter()
{
    delete _ser;
    deleteLater();
}

void AppWorker::onReceptionTrame(QString trame)
{
    cout << "Reception de : " << trame.toStdString() << endl;

    if(trame == "quit") {
        _ser->send("bye !");
        emit termine();
    }
}

void AppWorker::onNewConnection()
{
    // Récupère la socket du nouveau client connecté
    if( _client ==  nullptr) {
        _client = _monServeurTCP.nextPendingConnection();
        // Mettre en place les slots pour ce client
        connect(_client, &QTcpSocket::readyRead, this, &AppWorker::onReadyRead);
        connect(_client, &QTcpSocket::stateChanged, this, &AppWorker::onSocketStateChanged);

        // Dire au client qu'on est bien connecté au serveur
        qDebug() << "Client connecté";
    } else {
        qDebug() << "client déjà connecté. C'est complet !!";
    }
}

void AppWorker::onReadyRead()
{
    QTcpSocket* sender = static_cast<QTcpSocket*>(QObject::sender());
    QByteArray datas = sender->readAll();

    // Afficher trame reçue
    qDebug() << "Données reçues : " << QString(datas);

    // Extraire la commande de la trame (<- les 2 caractères après <STX>)
    int idxSTX = datas.indexOf('\x02');
    QString cmde = datas.mid(idxSTX+1, 2);
    if(cmde.size() != 0) {
        qDebug() << "cmde : " << cmde;
        uint8_t chks = datas.at(idxSTX+1) + datas.at(idxSTX+2) + datas.at(idxSTX+3);
        if( chks == 0x00) {
            QString cmdeArduino;
            if(cmde.at(0) == 'R') { /* déplacement robot */
                switch(cmde.at(1).toLatin1()) {
                case 'F' :
                    cmdeArduino = "A";
                    break;
                case 'B' :
                    cmdeArduino = "R";
                    break;
                case 'L' :
                    cmdeArduino = "G";
                    break;
                case 'R' :
                    cmdeArduino = "D";
                    break;
                case 'S' :
                    cmdeArduino = "S";
                    break;
                }
                _ser->send(cmdeArduino);
                _client->write("\x06"); // <ACK>
            } else if (cmde.at(0) == 'C') { /* action pince */
                switch(cmde.at(1).toLatin1()) {
                case 'O' :
                    cmdeArduino = "O";
                    break;
                case 'C' :
                    cmdeArduino = "C";
                    break;
                case 'S' :
                    cmdeArduino = "H";
                    break;
                }
                _ser->send(cmdeArduino);
                _client->write("\x06"); // <ACK>
            } else {
                qDebug() << "Commande inconnue";
            }
        } else {
            qDebug() << "Erreur de checksum";
        }
    } else {
        qDebug() << "Pas de trame valide trouvée";
    }
}

void AppWorker::onSocketStateChanged(QAbstractSocket::SocketState socketState)
{
    if (((socketState == QAbstractSocket::UnconnectedState) || (socketState == QAbstractSocket::ClosingState))
            && _client != nullptr)
    {
        _client->close();
        _client = nullptr;
        qDebug() << "Client déconnecté";
    }
}

